# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=sac-core
_pkgver=10.0.37-0
pkgver=${_pkgver/-/.}
pkgrel=12.1
pkgdesc='Safenet Authentication Client for Alladin eToken, stripped core package'
arch=('x86_64')
makedepends=('libarchive')
license=('custom')
install=${pkgname}.install
url='https://data-protection-updates.gemalto.com/category/safenet-authentication-client/'
source=('eToken.conf')
source_x86_64=("https://manjariando.gitlab.io/source-packages/safenetauthenticationclient/SafenetAuthenticationClient-BR-${_pkgver}_amd64.deb")
sha256sums=('85b850b820610e029428e577ca0e48f6fb7b4148ae8d702ca20b191963046c6c')
sha256sums_x86_64=('48dedf0a0d36abb0bb2faace8e17afa2ab37756136bddd6c303a4789bc8b844e')

build() {
    tar xf "${srcdir}/data.tar.xz"
}

package() {
    depends=('kmod' 'libusb-compat' 'nss' 'openssh' 'pcsclite')

    mkdir -p "${pkgdir}/usr/lib/pcsc/drivers"
    cp -dpr --no-preserve=ownership "${srcdir}/usr/share/eToken/drivers/aks-ifdh.bundle" "${pkgdir}/usr/lib/pcsc/drivers"
    rm "${pkgdir}/usr/lib/pcsc/drivers/aks-ifdh.bundle/Contents/Linux/readme.txt"

    mkdir -p "${pkgdir}/usr/lib"
    cp --no-preserve=ownership "${srcdir}/lib/libeToken.so.10.0.37" "${pkgdir}/usr/lib/libeToken.so.10.0.37"
    cp --no-preserve=ownership "${srcdir}/lib/libcardosTokenEngine.so.10.0.37" "${pkgdir}/usr/lib/libcardosTokenEngine.so.10.0.37"

    mkdir -p "${pkgdir}/etc"
    cp "${srcdir}/eToken.conf" "${pkgdir}/etc/eToken.conf"

    cd "${pkgdir}/usr/lib/"
    ln -sf libeToken.so.10.0.37 libeTPkcs11.so
    ln -sf libeToken.so.10.0.37 libeToken.so.10.0
    ln -sf libeToken.so.10.0.37 libeToken.so.10
    ln -sf libeToken.so.10.0.37 libeToken.so
    ln -sf libcardosTokenEngine.so.10.0.37 libcardosTokenEngine.so.10.0
    ln -sf libcardosTokenEngine.so.10.0.37 libcardosTokenEngine.so.10
    ln -sf libcardosTokenEngine.so.10.0.37 libcardosTokenEngine.so

    cd "${pkgdir}/usr/lib/pcsc/drivers/aks-ifdh.bundle/Contents/Linux/"
    ln -sf libAksIfdh.so.10.0 libAksIfdh.so
    ln -sf libAksIfdh.so.10.0 libAksIfdh.so.10
}
